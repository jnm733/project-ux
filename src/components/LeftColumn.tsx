import * as React from 'react';
import ListModelItem from '../containers/ListModelItem'
import ListCarUsedItem from '../containers/ListCarUsedItem'
import ListNewsItem from '../components/ListNewsItem'
import ModalContactForm from '../containers/ModalContactForm'

interface ILeftColumnProps {
    make: string;
}

interface ILeftColumnState {

}

class LeftColumn extends React.Component<ILeftColumnProps, ILeftColumnState> {
    constructor(props: ILeftColumnProps) {
        super(props);
    }

    public render() {
        return (
            <span className="principal">
                <section className="zona-contenido">
                    <h2 className="titulo-zona">Modelos actuales</h2>
                    <ul className="listados-marca">
                        <ListModelItem image={'audi-a1'} title={'Audi A1'} pvp={22.612} offer={17.489} versions={['A1 Citycarver']} />
                        <ListModelItem image={'audi-a3'} title={'Audi A3'} pvp={30.905} offer={23.198} versions={['A3 Cabrio', 'A3 RS', 'A3 S3']}/>
                        <ListModelItem image={'audi-a4'} title={'Audi A4'} pvp={38.046} offer={27.398} versions={['A4 Avant', '4 Berlina', 'RS4 Avant']}/>
                        <ListModelItem image={'audi-a5'} title={'Audi A5'} pvp={48.124} offer={35.654} versions={['A5 Cabrio', 'A5 Coupé', 'S5']} />
                        <ListModelItem image={'audi-a6'} title={'Audi A6'} pvp={55.032} offer={44.667} versions={['A6 Avant', 'A6 Berlina', 'RS6']} />
                        <ListModelItem image={'audi-a7'} title={'Audi A7'} pvp={66.702} offer={0} />
                        <ListModelItem image={'audi-a8'} title={'Audi A8'} pvp={101.076} offer={0} versions={['A8 S8']} />
                        <ListModelItem image={'audi-e-tron'} title={'Audi e-tron'} pvp={72.991} offer={0} />
                        <h2 className="titulo-zona">{this.props.make}s de Km 0</h2>
                        <ul className="used-cars-list">
                            <ListCarUsedItem image={'audi-a3'} title={'Audi A3 Design 35 TFSI 110kW 150CV'} pvp={"27.500"} info={"1.000kms | 2018 | Madrid"} />
                            <ListCarUsedItem image={'audi-q2'} title={'Audi Q2 Design 30 TDI 85kW 116CV'} pvp={"27.900"} info={"500kms | 2019 | Barcelona"} />
                            <ListCarUsedItem image={'audi-q3'} title={'Audi Q3 2.0TDI Design edition 110kW'} pvp={"27.650"} info={"2.300kms | 2019 | Valencia"} />
                            <ListCarUsedItem image={'audi-a5'} title={'Audi A5 1.4 TFSI 110kW 150CV S tron'} pvp={"34.900"} info={"750kms | 2018 | Lleida"} />
                        </ul>
                        <ListModelItem image={'audi-q2'} title={'Audi Q2'} pvp={29.016} offer={22.115} />
                        <ListModelItem image={'audi-q3'} title={'Audi Q3'} pvp={36.465} offer={28.341} versions={['Q3 RS']} />
                        <ListModelItem image={'audi-q5'} title={'Audi Q5'} pvp={44.905} offer={37.221} versions={['Q5 SQ5']} />
                        <ListModelItem image={'audi-q7'} title={'Audi Q7'} pvp={70.913} offer={60.042} />
                        <ListModelItem image={'audi-q8'} title={'Audi Q8'} pvp={82.559} offer={72.456} />
                        <ListModelItem image={'audi-r8'} title={'Audi R8'} pvp={201.293} offer={190.778} versions={['R8 Coupé', 'R8 Spyder']} />
                        <ListModelItem image={'audi-tt'} title={'Audi TT'} pvp={43.389} offer={36.518} versions={['TT Coupé', 'TT Roadster', 'TT RS']}/>
                        <h2 className="titulo-zona">{this.props.make}s de Segunda Mano</h2>
                        <ul className="used-cars-list">
                            <ListCarUsedItem image={'audi-a1'} title={'Audi A1 1.6 TDI 105cv Attraction'} pvp={"8.500"} info={"128.000kms | 2011 | Madrid"} />
                            <ListCarUsedItem image={'audi-q7'} title={'Audi Q7 3.0 TDI 245cv quattro tiptronic'} pvp={"32.900"} info={"50.000kms | 2019 | Cádiz"} />
                            <ListCarUsedItem image={'audi-a4'} title={'Audi A4 Allroad Quattro 2.0 TDI 190CV'} pvp={"24.900"} info={"70.300kms | 2015 | Valencia"} />
                            <ListCarUsedItem image={'audi-tt'} title={'Audi TT Coupe 1.8 TFSI 160cv'} pvp={"10.300"} info={"200.000kms | 2010 | Huelva"} />
                        </ul>
                    </ul>
                </section>
                <section className="zona-contenido">
                    <h2 className="titulo-zona">Futuros modelos</h2>
                    <ul className="listados-marca">
                        <ListModelItem image={'audi-a9'} title={'Audi A9'} pvp={0} offer={0} description={'El nuevo Audi A9 se situará como la berlina tope de gama de la firma alemana. Un buque insignia que se situará como la alternativa más lujosa y exclusiva de Audi para su segmento. Anticipado mediante algunos prototipos,...'} />
                        <ListModelItem image={'audi-q1'} title={'Audi Q1'} pvp={0} offer={0} description={'La familia SUV de Audi dará la bienvenida muy pronto a su integrante más pequeño y situado como opción de acceso. El Audi Q1. Mientras que la firma de Ingolstadt finaliza el desarrollo de la segunda generación del A1,...'} />
                        <ListModelItem image={'audi-q4'} title={'Audi Q4'} pvp={0} offer={0} description={'Tras llegar a un acuerdo con FCA para poder usar la denominación Q4, los chicos de Audi están decididos a lanzar un SUV Coupé de aspecto deportivo y con una mecánica eléctrica o híbrida enchufable. El Audi Q4 estará...'} />
                        <ListModelItem image={'audi-q6'} title={'Audi Q6'} pvp={0} offer={0}/>
                    </ul>
                </section>
                <h4 className="titulo-pagina">Mundo {this.props.make}</h4>
                <p className="descripcion-pagina">Todas las noticias, pruebas, fotos espía y rumores de {this.props.make}</p>
                <section className="zona-contenido">
                    <h4 className="titulo-zona"><span>Pruebas {this.props.make}</span></h4>
                    <ListNewsItem image={'noticia-1'} title={'Prueba Audi A6 Avant 50 TDI Quattro, el viajero incansable'} tags={['Audi A6', 'Audi A6 Avant', 'Pruebas Audi']} description={'Probamos el Audi A6 Avant en su versión diésel semihíbrida con distintivo Eco. El station wagon alemán brilla por equipamiento, confort, espacio y consumo y se propone como...'} author={'Oscar Magro'}/>
                    <ListNewsItem image={'noticia-2'} title={'Prueba Audi A4 35 TFSI, ¿será suficiente con la versión de acceso?'} tags={['Audi A4', 'Berlinas', 'Coches nuevos', 'Pruebas Audi']} description={'El Audi A4 ha ido mejorando con el paso de los años. Poco a poco ha logrado situarse como referencia del segmento superando a sus más duros rivales....'} author={'Javier Gómara'} />
                    <ListNewsItem image={'noticia-3'} title={'Prueba Audi A4 2020, la lucha por mantenerse a la cabeza'} tags={['Audi A4', 'Berlinas', 'Coches nuevos', 'Pruebas Audi']} description={'Ser el mejor no vale cuando la competencia te supera, así que hay que seguir evolucionando. Eso mismo le ha pasado al Audi A4 2020, que ante la...'} author={'Javier Gómara'} />
                    <footer className="cierre-zona-contenido"><a href="#" title="Ver más pruebas {this.props.make}">Ver más pruebas {this.props.make}</a></footer>
                </section>
                <section className="zona-contenido">
                    <h4 className="titulo-zona"><span>Últimas noticias {this.props.make}</span></h4>
                    <ListNewsItem image={'noticia-4'} title={'Audi anuncia su alineación de pilotos para las 12 Horas de Bathurst'} description={'Hasta ocho pilotos de fábrica competirán en los tres Audi R8 LMS GT3 Evo que alineará el Audi Team Valvoline La apuesta de la marca de Ingolstadt en...'} author={'Fernando Sancho'} />
                    <ListNewsItem image={'noticia-5'} title={'El Salón de Nueva York 2020 pierde fuelle: Audi no estará presente'} description={'Audi ha decidido no acudir al Salón del Automóvil de Nueva York 2020 que tendrá lugar en el mes de abril. De esta manera, la icónica marca de...'} author={'Antonio Fernández'}/>
                    <ListNewsItem image={'noticia-6'} title={'El Team WRT repite en el GT World Challenge Europe como socio de Audi'} description={'La estructura belga Team WRT ha confirmado su continuidad en la parrilla del GT World Challenge Europe para la temporada 2020. La formación competirá tanto en los eventos...'} author={'Fernando Sancho'} />
                    <footer className="cierre-zona-contenido"><a href="#" title="Ver más noticias {this.props.make}">Ver más noticias {this.props.make}</a></footer>
                </section>
                <section className="zona-contenido">
                    <h4 className="titulo-zona"><span>Fotos espía {this.props.make}</span></h4>
                    <ListNewsItem image={'noticia-7'} title={'El lavado de cara del Audi Q2 2021 posa en sus primeras fotos espía'} tags={['Audi Q2', 'Fotos Espía', 'SUV', 'SUV Audi']} description={'Con prácticamente toda la gama renovada, Audi comienza las pruebas para actualizar el Q2 de cara a 2021, el SUV que ofrece a medio camino entre el segmento...'} author={'Fran Romero'}/>
                    <ListNewsItem image={'noticia-8'} title={'El nuevo Audi S3 Sedán 2020 ya se encuentra en el norte de Europa'} tags={['Audi A3', 'Audi A3 Sedán', 'Fotos Espía']}  description={'Una vez más el nuevo Audi S3 Sedán 2020 ha sido fotografiado a plena luz del día. En esta ocasión, la nueva generación de la versión deportiva del...'} author={'Antonio Fernández'}/>
                    <ListNewsItem image={'noticia-9'} title={'¡Sin camuflaje! El nuevo Audi A3 Sedán 2020 cazado al descubierto'} tags={['Audi A3', 'Audi A3 Sedán', 'Fotos Espía']}  description={'La nueva generación del Audi A3 Sedán ha sido fotografiada sin una pizca de camuflaje. Buena parte de su frontal queda al descubierto en estas fotos espía. El...'} author={'Antonio Fernández'}/>
                    <footer className="cierre-zona-contenido"><a href="#" title="Ver más fotos espía {this.props.make}">Ver más fotos espía {this.props.make}</a></footer>
                </section>
                <ModalContactForm />
            </span>
    );
    }
    }

    export default LeftColumn;

export function openModal() {
    var modal = document.getElementById('contacto_modal');
    if (modal)
    {
        modal.classList.add('visible');
        document.body.classList.add('noscroll');
    }
}