import * as React from 'react';

interface IModalContactFormProps {
    modal: boolean
    onModalClick: (modal: boolean) => any;
}

interface IModalContactFormState {

}

class ModalContactForm extends React.Component<IModalContactFormProps, IModalContactFormState> {
    constructor(props: IModalContactFormProps) {
        super(props);
    }

    public render() {
        const onClick = () => {
            this.props.onModalClick(!!!this.props.modal)
        }
        return (
            <div id="contacto_modal" className={"modal-window " + (this.props.modal ? 'visible' : '')}>
                <div className="modal">
                    <a title="Close" className="modal-close" onClick={onClick}>X</a>
                    <h3 className="titulo-pagina">Solicitar oferta</h3>
                    <p className="descripcion-pagina">Contactaremos contigo y te enviaremos un email con la mejor oferta para este vehículo</p>
                    <form id="solicitar_oferta_form" className="formulario solicitar-oferta" method="post" action="#">
                        <span className="campo">
                            <input name="nombre_apellidos" title="Debe contener letras" className="texto" type="text" placeholder="Nombre y apellidos"></input>
                        </span>
                        <span className="campo">
                            <input name="telefono" title="Debe tener un total de 9 dígitos" className="texto" type="text" placeholder="Teléfono "></input>
                        </span>
                        <span className="campo">
                            <input name="email" type="email" title="Debe tener un formato válido" className="texto" placeholder="Email"></input>
                        </span>
                        <span className="campo">
                            <input name="cp" title="Debe tener un total de 5 dígitos" className="texto" type="text" placeholder="Código postal"></input>
                        </span>
                        <span id="condiciones_block" className="campo">
                            <span className="check-radio">
                                <input type="checkbox" id="acepto_condiciones" name="acepto_condiciones"></input>
                                <label htmlFor="acepto_condiciones">
                                    <span className="icono"><svg viewBox="0 0 24 24"><path d="M19,3l-14,0c-1.11,0 -2,0.89 -2,2l0,14c0,1.097 0.903,2 2,2l14,0c1.097,0 2,-0.903 2,-2l0,-14c0,-1.11 -0.9,-2 -2,-2m0,2l0,14l-14,0l0,-14l14,0Z"></path><rect x="7.034" y="7.067" width="9.964" height="9.964" className="marcado"></rect></svg></span>
                                    <span className="label">He leído y acepto el <a href="#" title="Aviso legal">Aviso legal</a> y las <a href="#" title="Condiciones de uso">Condiciones de uso</a></span>
                                </label>
                            </span>
                        </span>
                        <button id="submit_oferta_form" className="boton acento-solido" type="submit">
                            <span className="accion">Solicitar oferta</span>
                        </button>
                        <div className="mensaje-form-completo "><span
                            className="titulo-mensaje">Oferta solicitada</span><span className="descripcion">Muy pronto nos pondremos en contacto contigo para hacerte llegar las mejores ofertas</span><span
                            className="mas-opciones"><span>Mira el precio del seguro aquí mismo</span><a
                            className="boton acento-solido" target="_blank" rel="nofollow"
                            href="#"><span
                            className="accion">Ver</span></a></span></div>
                    </form>
                </div>
            </div>
        );
    }
}

export default ModalContactForm;