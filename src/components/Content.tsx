import * as React from 'react';
import RightColumn from '../containers/RightColumn'
import LeftColumn from '../containers/LeftColumn'
import BannerHorizontal from '../components/BannerHorizontal'

interface IContentProps {
    make: string;
}

interface IContentState {

}

class Content extends React.Component<IContentProps, IContentState> {
    constructor(props: IContentProps) {
        super(props);
    }

    public render() {
        return (
            <>
            <BannerHorizontal />
            <main className="main">
                <span className="encabezado-pagina"><span className="contenedor-miga">
                    <ul className="miga">
                    <li><a href="#" title="Inicio">Inicio</a></li>
                    <li><a href="#" title="Fichas técnicas">Fichas técnicas</a></li>
                    <li>{this.props.make}</li>
                    </ul></span>
                    <ul className="tira-marcas">
                        <li><a className={this.props.make == 'Alfa Romeo' ? "activo" : "" } href="#" title="Alfa Romeo"><img src="img/marcas/alfa-romeo-small.jpg" alt="Alfa Romeo"></img></a></li>
                        <li><a className={this.props.make == 'Audi' ? "activo" : "" } href="#" title="Audi"><img src="img/marcas/audi-small.jpg" alt="{this.props.make}"></img></a></li>
                        <li><a className={this.props.make == 'BMW' ? "activo" : "" } href="#" title="BMW"><img src="img/marcas/bmw-small.jpg" alt="BMW"></img></a></li>
                        <li><a className={this.props.make == 'Chevrolet' ? "activo" : "" } href="#" title="Chevrolet"><img src="img/marcas/chevrolet-small.jpg" alt="Chevrolet"></img></a></li>
                        <li><a className={this.props.make == 'Citroen' ? "activo" : "" } href="#" title="Citroen"><img src="img/marcas/citroen-small.jpg" alt="Citroen"></img></a></li>
                        <li><a className={this.props.make == 'Mercedes' ? "activo" : "" } href="#" title="Mercedes"><img src="img/marcas/mercedes-small.jpg" alt="Mercedes"></img></a></li>
                        <li><a className={this.props.make == 'Fiat' ? "activo" : "" } href="#" title="Fiat"><img src="img/marcas/fiat-small.jpg" alt="Fiat"></img></a></li>
                        <li><a className={this.props.make == 'Ford' ? "activo" : "" } href="#" title="Ford"><img src="img/marcas/ford-small.jpg" alt="Ford"></img></a></li>
                        <li><a className={this.props.make == 'Skoda' ? "activo" : "" } href="#" title="Skoda"><img src="img/marcas/skoda-small.jpg" alt="Skoda"></img></a></li>
                        <li><a className={this.props.make == 'Renault' ? "activo" : "" } href="#" title=""><img src="img/marcas/renault-small.jpg" alt="Renault"></img></a></li>
                        <li><a className={this.props.make == 'Nissan' ? "activo" : "" } href="#" title="Nissan"><img src="img/marcas/nissan-small.jpg" alt="Nissan"></img></a></li>
                        <li><a className={this.props.make == 'Toyota' ? "activo" : "" } href="#" title="Toyota"><img src="img/marcas/toyota-small.jpg" alt="Toyota"></img></a></li>
                        <li><a className={this.props.make == 'Mazda' ? "activo" : "" } href="#" title="Mazda"><img src="img/marcas/mazda-small.jpg" alt="Mazda"></img></a></li>
                        <li><a className={this.props.make == 'Honda' ? "activo" : "" } href="#" title="Honda"><img src="img/marcas/honda-small.jpg" alt="Honda"></img></a></li>
                        <li><a className={this.props.make == 'Volkswagen' ? "activo" : "" } href="#" title="Volkswagen"><img src="img/marcas/volkswagen-small.jpg" alt="Volkswagen"></img></a></li>
                        <li><a className={this.props.make == 'Lexus' ? "activo" : "" } href="#" title="Lexus"><img src="img/marcas/lexus-small.jpg" alt="Lexus"></img></a></li>
                        <li><a className={this.props.make == 'Seat' ? "activo" : "" } href="#" title="Seat"><img src="img/marcas/seat-small.jpg" alt="Seat"></img></a></li>
                        <li><a className={this.props.make == 'Mini' ? "activo" : "" } href="#" title="Mini"><img src="img/marcas/mini-small.jpg" alt="Mini"></img></a></li>
                    </ul>
                    <h1 className="titulo-pagina">{this.props.make}</h1>
                    <p className="descripcion-pagina">Precios, modelos y futuros modelos de <strong>{this.props.make}</strong> que actualmente puedes <strong>comprar en España</strong></p>
                </span>
                <span className="columnas">
                    <LeftColumn />
                    <RightColumn history={'Cuatro empresas son las responsables del nacimiento de Audi. NSU aparece en 1873 y se dedica, inicialmente, a la fabricación de máquinas tejedoras: el primero de los cuatro aros acaba de ver la luz. A finales de la década, la empresa se traslada de Riedlingen a Neckarsulm, Alemania.'} />
                </span>
            </main>
            <BannerHorizontal />
            </>
        );
    }
}

export default Content;