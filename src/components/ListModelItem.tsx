import * as React from 'react';

interface IListModelItemProps {
    title: string,
    pvp: number,
    offer: number,
    description?: string,
    versions?: String[],
    image: string,
    modal: boolean,
    onModalClick: (modal: boolean) => any;
}

interface IListModelItemState {

}

class ListModelItem extends React.Component<IListModelItemProps, IListModelItemState> {
    constructor(props: IListModelItemProps) {
        super(props);
    }

    public render() {
        const onClick = () => {
            this.props.onModalClick(!!!this.props.modal)
        }
        return (
            <li>
                <a className="imagen" href="#" title={this.props.title}>
                    <img src={'img/modelos/'+this.props.image+'.jpg'} alt={this.props.title}></img>
                </a>
                <span className="textos">
                    <h3 className="nombre-coche">
                        <a href="#" title={this.props.title}>{this.props.title}</a>
                    </h3>
                    {this.props.versions ?
                        <ul>
                            {this.props.versions.map(version => <li><a href="#">{version}</a></li>)}
                        </ul>
                        : null}
                    {this.props.description ?
                        <p>{this.props.description}</p>
                        : null}
                </span>
                {this.props.pvp > 0 ?
                    <a className="solicitar-oferta-link" onClick={onClick}>
                        <span className="precio">Desde&nbsp;
                            <span className="original">{this.props.pvp} €</span>
                            {this.props.offer > 0 ?
                                <span className="oferta"> {this.props.offer} €<span className="icono">
                                    <svg viewBox="0 0 24 24"><path d="M8.59,16.58L13.17,12L8.59,7.41L10,6L16,12L10,18L8.59,16.58Z"></path></svg></span>
                                </span>
                                : null}
                        </span>
                        <span className="solicitar-oferta-label">Solicitar oferta</span>
                    </a>
                    : null}

            </li>
    );
    }
    }

    export default ListModelItem;