import * as React from 'react';
import banner_alto from '../assets/img/banners/robapaginas-alto.jpg';
import logo_facebook from '../assets/img/rrss/logo-facebook.svg';
import logo_twitter from '../assets/img/rrss/logo-twitter.svg';
import logo_youtube from '../assets/img/rrss/logo-youtube.svg';
import imagen_medidas from '../assets/img/audi-a1-hatchback.jpg';
import gallery_img from '../assets/img/galleries/pascal-wehrlein-1.jpg';
import BannerVertical from '../components/BannerVertical'


interface IRightColumnProps {
    history: string,
    make: string
}

interface IRightColumnState {

}

class RightColumn extends React.Component<IRightColumnProps, IRightColumnState> {
    constructor(props: IRightColumnProps) {
        super(props);
    }

    public render() {
        return (
            <span className="secundaria">
                <span className="divisor">
                    <section className="zona-contenido">
                        <h4 className="titulo-zona">Historia de {this.props.make}</h4>
                        <p>{this.props.history}</p><a
                        href="#" title="Más información">Más información</a>
                    </section>
                </span>

                <BannerVertical />

                <span className="divisor">
                    <aside className="redes-sociales">
                        <h5 className="titulo-modulo">Síguenos en...</h5>
                        <a href="#" title="Disfruta de los mejores vídeos de motor.es">
                            <img src={logo_youtube} alt="YouTube"></img><span>Disfruta de los mejores <strong>vídeos de motor.es</strong></span></a>
                        <a href="#" title="Ya somos más de 200.000 amigos en Facebook">
                            <img src={logo_facebook} alt="Facebook"></img>
                            <span>Ya somos más de <strong>200.000 amigos</strong> en Facebook</span>
                        </a>
                        <a href="#" title="Actualidad concentrada en un tweet">
                            <img src={logo_twitter} alt="twitter"></img>
                            <span>Actualidad <strong>concentrada en un tweet</strong></span>
                        </a>
                    </aside>
                </span>

                <BannerVertical />

                <span className="divisor">
                    <aside className="modulo-suscripcion">
                        <h5 className="titulo-modulo">Todas las noticias del motor en tu email</h5>
                        <form>
                            <legend>Recibe nuestro boletín semanal</legend>
                            <input className="texto" type="text"></input>
                            <button>Enviar</button>
                            <label className="check" htmlFor="aviso_legal">
                                <input type="checkbox"></input>
                                <span>Al suscribirte aceptas el <a href="#" title="Aviso legal">Aviso legal</a> y la <a href="#" title="Política de privacidad">Política de privacidad</a></span>
                            </label>
                        </form>
                    </aside>
                </span>

                <BannerVertical />

                <span className="divisor">
                    <aside className="modulo-basico">
                        <h4 className="titulo-modulo">Medidas de coches {this.props.make}</h4>
                        <a className="imagen" href="#" title="Medidas de coches Audi">
                            <img src={imagen_medidas} alt="Medidas de coches Audi"></img>
                        </a>
                        <footer><a href="#" title="Ver todas">Ver todas</a></footer>
                    </aside>
                </span>

                <BannerVertical />

                <span className="divisor">
                    <aside className="modulo-galeria">
                        <span className="antetitulo">Galería de fotos</span>
                        <h5 className="titulo-galeria"><a href="#" title="Fotos Pascal Wehrlein F1 2017">Fotos Pascal Wehrlein F1 2017</a></h5>
                        <a className="imagenes" href="#" title="Fotos Pascal Wehrlein F1 2017">
                            <img src={gallery_img} alt="Pascal Wehrlein"></img>
                        </a><footer className="enlaces">
                        <a href="#" title="Fotos Audi R8 V10 RWD y un poco de texto más para ver que pasa cuando dobla">
                            <span className="icono"><svg viewBox="0 0 24 24"><path d="M17.332,1l0,3.882l6.668,0l0,18.118l-24,0l0,-18.118l6.667,0l0,-3.882l10.665,0Zm4,6.471l-18.667,0l0,12.941l18.667,0l0,-12.941Zm-13.333,6.47c0,-2.148 1.787,-3.882 4,-3.882c2.214,0 4,1.734 4,3.882c0,2.149 -1.786,3.883 -4,3.883c-2.213,0 -4,-1.734 -4,-3.883Zm6.668,-10.353l-5.334,0l0,1.294l5.334,0l0,-1.294Z"></path></svg></span>Fotos Audi R8 V10 RWD y un poco de texto más para ver que pasa cuando dobla</a><a href="#" title="Nueva gama S de Audi y un poco de texto más para ver que pasa cuando dobla"><span className="icono"><svg viewBox="0 0 24 24"><path d="M17.332,1l0,3.882l6.668,0l0,18.118l-24,0l0,-18.118l6.667,0l0,-3.882l10.665,0Zm4,6.471l-18.667,0l0,12.941l18.667,0l0,-12.941Zm-13.333,6.47c0,-2.148 1.787,-3.882 4,-3.882c2.214,0 4,1.734 4,3.882c0,2.149 -1.786,3.883 -4,3.883c-2.213,0 -4,-1.734 -4,-3.883Zm6.668,-10.353l-5.334,0l0,1.294l5.334,0l0,-1.294Z"></path></svg></span>Nueva gama S de Audi y un poco de texto más para ver que pasa cuando dobla</a></footer>
                    </aside>
                </span>

                <span className="divisor">
                    <aside className="modulo-basico listado">
                        <h4 className="titulo-modulo">Otros modelos</h4>
                        <ul>
                            <li><a href="#" title="Audi Q1">Audi Q1</a></li>
                        </ul>
                    </aside>
                </span>
            </span>
        );
    }
}

export default RightColumn;