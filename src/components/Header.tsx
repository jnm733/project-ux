import * as React from 'react';
import logo_ab from '../assets/img/auto-bild.jpg';
import logo_motor from '../assets/img/motor-es-negativo.svg';

interface IHeaderProps {
    collapsed: boolean,
    onLateralMenuClick: (collapse: boolean) => any;
}

interface IHeaderState {
}

class Header extends React.Component<IHeaderProps, IHeaderState> {
    constructor(props: IHeaderProps) {
        super(props);
    }

    public render() {
        const onClick = () => {
            this.props.onLateralMenuClick(!!!this.props.collapsed)
        }
        return (
            <div>
            <div className="dv_precabecera">
                <div className="precabecera"><a className="favoritos"
                                                href="https://www.motor.es/maketa/portada-marca.html#"
                                                title="Mis favoritos"><span className="icono"><svg viewBox="0 0 24 24"><path
                    d="M14.847,8.935l7.153,0.705l-5.393,4.99l1.573,7.37l-6.18,-3.851l-6.18,3.851l1.573,-7.37l-5.393,-4.99l7.153,-0.705l2.847,-6.935l2.847,6.935Zm-4.422,1.702l-4.218,0.416l3.185,2.946l-0.895,4.188l3.503,-2.182l3.502,2.182l-0.894,-4.188l3.185,-2.946l-4.218,-0.416l-1.575,-3.838c-0.525,1.28 -1.05,2.559 -1.575,3.838l0,0Z"></path></svg></span>Mis
                    favoritos</a><a className="acceso" href="https://www.motor.es/maketa/portada-marca.html#"
                                    title="Acceso">Acceso</a>
                    <a className="boton" href="https://www.motor.es/maketa/portada-marca.html#"><span
                        className="accion">Vender coche</span>
                    </a>
                    <form className="buscador">
                        <input type="text" placeholder="Busca noticias, pruebas..."></input>
                            <button><span className="icono"><svg viewBox="0 0 24 24"><path
                                d="M12.898,15.218c-0.251,0.148 -0.511,0.28 -0.78,0.394c-0.78,0.33 -1.623,0.504 -2.469,0.515c-0.85,0.011 -1.7,-0.143 -2.491,-0.454c-0.762,-0.302 -1.465,-0.746 -2.065,-1.304c-0.577,-0.539 -1.058,-1.181 -1.41,-1.889c-0.3,-0.603 -0.506,-1.251 -0.607,-1.917c-0.105,-0.688 -0.101,-1.393 0.013,-2.08c0.102,-0.61 0.29,-1.206 0.559,-1.764c0.643,-1.333 1.738,-2.429 3.071,-3.072c0.558,-0.268 1.154,-0.457 1.765,-0.559c0.301,-0.05 0.605,-0.077 0.911,-0.086c0.085,-0.002 0.169,-0.002 0.254,-0.002c0.306,0.006 0.611,0.029 0.914,0.075c0.613,0.094 1.212,0.276 1.773,0.538c1.269,0.593 2.331,1.593 2.999,2.823c0.307,0.565 0.53,1.176 0.659,1.806c0.145,0.708 0.172,1.44 0.081,2.157c-0.085,0.67 -0.274,1.326 -0.561,1.937c-0.089,0.192 -0.189,0.379 -0.296,0.561c1.798,1.737 3.534,3.535 5.301,5.302c0.054,0.056 0.107,0.114 0.154,0.176c0.117,0.157 0.206,0.334 0.26,0.521c0.05,0.169 0.072,0.346 0.066,0.521c-0.016,0.453 -0.224,0.886 -0.565,1.182c-0.118,0.103 -0.251,0.188 -0.393,0.253c-0.178,0.081 -0.37,0.13 -0.566,0.144c-0.214,0.015 -0.43,-0.012 -0.635,-0.08c-0.224,-0.075 -0.424,-0.198 -0.599,-0.356c-1.81,-1.747 -3.558,-3.557 -5.337,-5.335l-0.006,-0.007l0,0Zm-3.393,-10.166c-0.211,0.003 -0.421,0.019 -0.63,0.051c-0.42,0.064 -0.83,0.189 -1.215,0.369c-0.906,0.423 -1.659,1.15 -2.115,2.04c-0.194,0.377 -0.333,0.781 -0.412,1.196c-0.098,0.508 -0.108,1.034 -0.029,1.545c0.066,0.438 0.199,0.866 0.391,1.264c0.237,0.491 0.563,0.937 0.957,1.312c0.408,0.389 0.887,0.701 1.408,0.914c0.558,0.229 1.16,0.341 1.763,0.333c0.6,-0.008 1.196,-0.135 1.746,-0.375c0.515,-0.226 0.987,-0.549 1.385,-0.947c0.385,-0.385 0.7,-0.839 0.925,-1.335c0.183,-0.405 0.305,-0.838 0.362,-1.278c0.067,-0.532 0.041,-1.075 -0.08,-1.597c-0.095,-0.411 -0.249,-0.808 -0.455,-1.176c-0.479,-0.857 -1.238,-1.548 -2.137,-1.941c-0.39,-0.172 -0.806,-0.286 -1.228,-0.34c-0.211,-0.026 -0.423,-0.037 -0.636,-0.035l0,0Z"></path></svg></span>
                            </button>
                    </form>
                    <a className="autobild" href="https://www.motor.es/maketa/portada-marca.html#"
                       title="autobild.es"><img
                        src={logo_ab} alt="autobild.es"></img></a>
                </div>
            </div>

                <nav className="navegacion"><span className="caja"><span className="primer-nivel">
                    <a onClick={onClick} className="icon-menu" href="#" title="Menú" id="expand_menu_btn"><span className="icono"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><path fill="#fff" d="M24 7h-24v-2h24v4zm0 4h-24v2h24v-4zm0 6h-24v2h24v-4z"></path></svg></span></a>
                    <a className="logo-motor" href="/" title="motor.es"><img src={logo_motor}
                                                                          alt="motor.es"></img></a>
<ul>
<li><a id="home" href="#" title="Home">Home</a></li>
<li><a id="noticias" href="#" title="Noticias">Noticias</a></li>
<li><a id="formula_1" href="#" title="Fórmula 1">Fórmula 1</a></li>
<li><a id="coches_segunda_mano" href="#" title="Coches segunda mano">Coches segunda mano</a></li>
<li><a id="coches_km_0" href="#" title="Coches KM 0">Coches KM 0</a></li>
<li><a id="coches_nuevos" href="#" title="Coches nuevos" className="activo">Coches nuevos</a></li>
<li><a id="seguros" href="#" title="Seguros">Seguros</a></li>
</ul></span></span>
                    <ul className="segundo-nivel">
                        <li><a id="fichas_tecnicas" href="#" title="Fichas técnicas" className="activo">Fichas técnicas</a>
                        </li>
                        <li><a id="pruebas_coches" href="#" title="Pruebas coches">Pruebas coches</a></li>
                        <li><a id="fotos_espia" href="#" title="Fotos espía">Fotos espía</a></li>
                        <li><a id="ventas_coche" href="#" title="Ventas coche">Ventas coche</a></li>
                        <li><a id="salones" href="#" title="Salones">Salones</a></li>
                        <li><a id="proximos_lanzamientos" href="#" title="Próximos lanzamientos">Próximos lanzamientos</a>
                        </li>
                        <li><a id="motorsport" href="#" title="Motorsport">Motorsport</a></li>
                        <li><a id="mas_noticias" href="#" title="Más">Más</a></li>
                    </ul>
                </nav>

                <section className={"expand-menu-section " + (!this.props.collapsed ? 'open' : '')} id="expand_menu_section"><a onClick={onClick}
                    className="expand_menu_close expand-menu-toggle" href="#" title="Cerrar menú"></a>
                    <div className="expand-menu">
                        <div className="menu-header"><a onClick={onClick} className="expand_menu_close item" href="#"
                                                        title="Cerrar menú"></a></div>
                        <div className="menu-content">
                            <div className="items-group"><p className="group-title">Es noticia...</p>
                                <ul className="list-items list list-tags">
                                    <li className="item"><a title="CES 2020, Consumer Electronics Show" href="https://www.motor.es/tag/ces">CES 2020</a></li>
                                    <li className="item"><a title="Ventas de coches en España 2019" href="https://www.motor.es/tag/ventas-coches-espana">Ventas 2019</a></li>
                                    <li className="item"><a title="Toyota GR Yaris" href="https://www.motor.es/noticias/toyota-gr-yaris-202063883.html">Toyota GR Yaris</a></li>
                                    <li className="item" ><a title="" href="https://www.motor.es/noticias/dakar-2020-donde-ver-como-seguir-201963579.html">Dónde ver Dakar 2020</a></li>
                                    <li className="item"><a title="" href="https://www.motor.es/nissan/qashqai/noticias">Nissan Qashqai 2021</a></li>
                                    <li className="item"><a title="" href="https://www.motor.es/noticias/seat-leon-2020-adelanto-201963399.html">Nuevo SEAT León</a></li>
                                    <li className="item"><a title="" href="https://www.motor.es/motorsport/dakar/">Dakar 2020</a></li>
                                    <li className="item"><a title="" href="https://www.motor.es/tag/coches-electricos">Coches eléctricos</a></li>
                                </ul>
                            </div>
                            <div className="items-group"><p className="group-title">No te pierdas</p>
                                <ul className="list-items  two-columns">
                                    <li className="item"><a title="" href="https://www.motor.es/tag/suv">SUV</a></li>
                                    <li className="item"><a title="" href="https://www.motor.es/tag/coches-electricos">Coches eléctricos</a></li>
                                    <li className="item"><a title="" href="https://www.motor.es/tag/ventas-coches">Datos de ventas</a></li>
                                    <li className="item"><a title="" href="https://www.motor.es/salon-del-automovil/">Salones</a></li>
                                    <li className="item"><a title="" href="https://www.motor.es/coches-baratos/">Coches baratos</a></li>
                                    <li className="item"><a title="" href="https://www.motor.es/coches-sin-carnet/">Coches sin carné</a></li>
                                    <li className="item"><a title="" href="https://www.motor.es/coches-nuevos/">Coches nuevos</a></li>
                                    <li className="item"><a title="" href="https://www.motor.es/coches-segunda-mano/">Coches ocasión</a></li>
                                    <li className="item"><a title="" href="https://www.motor.es/concesionarios/">Concesionarios</a></li>
                                    <li className="item"><a title="" href="https://www.motor.es/coches-km0/">Coches Km 0</a></li>
                                    <li className="item"><a title="" href="https://www.motor.es/comparar-coches/">Comparador de coches</a></li>
                                    <li className="item"><a title="" href="https://www.motor.es/medidas-coches">Medidas de coches</a></li>
                                    <li className="item"><a title="" href="https://www.motor.es/marcas/">Marcas de coches</a></li>
                                    <li className="item"><a title="" href="https://www.motor.es/galerias/">Galerías de coches</a></li>
                                    <li className="item"><a title="" href="https://www.motor.es/que-es/">Diccionario</a></li>
                                    <li className="item"><a title="" href="https://www.motor.es/tag/practicos">Prácticos</a></li>
                                    <li className="item"><a title="Recreaciones de coches" href="https://www.motor.es/tag/recreaciones">Recreaciones</a></li>
                                    <li className="item"><a title="Guías de compra de coche" href="https://www.motor.es/tag/guia-de-compra">Guías de compra</a></li>
                                </ul>
                            </div>
                            <div className="items-group"><p className="group-title">Síguenos</p>
                                <ul className="list-items list list-icons">
                                    <li className="item"><a target="_blank" rel="nofollow" className="facebook" href="https://www.facebook.com/motorpuntoes" title="en Facebook"><span className="icono"><svg viewBox="0 0 24 24"><path d="M16.5,3l0,3.375l-2.25,0c-0.617,0 -1.125,0.508 -1.125,1.125l0,2.25l3.375,0l0,3.375l-3.375,0l0,7.875l-3.375,0l0,-7.875l-2.25,0l0,-3.375l2.25,0l0,-2.813c0,-2.182 1.766,-3.937 3.938,-3.937l2.812,0Z"></path></svg></span></a></li>
                                    <li className="item"><a target="_blank" rel="nofollow" className="twitter" href="https://twitter.com/motorpuntoes" title="en Twitter"><span className="icono"><svg viewBox="0 0 24 24"><path d="M21,6.265c-0.663,0.309 -1.377,0.511 -2.117,0.609c0.758,-0.468 1.343,-1.209 1.618,-2.1c-0.714,0.441 -1.506,0.75 -2.34,0.926c-0.68,-0.759 -1.635,-1.2 -2.719,-1.2c-2.022,0 -3.674,1.694 -3.674,3.785c0,0.3 0.034,0.591 0.094,0.865c-3.063,-0.159 -5.79,-1.668 -7.606,-3.953c-0.318,0.556 -0.499,1.209 -0.499,1.897c0,1.315 0.645,2.48 1.644,3.141c-0.611,0 -1.179,-0.176 -1.678,-0.441l0,0.027c0,1.835 1.273,3.37 2.96,3.714c-0.31,0.089 -0.637,0.133 -0.973,0.133c-0.232,0 -0.464,-0.027 -0.688,-0.071c0.465,1.491 1.815,2.603 3.442,2.629c-1.257,1.024 -2.848,1.624 -4.586,1.624c-0.293,0 -0.585,-0.018 -0.878,-0.053c1.635,1.077 3.579,1.703 5.662,1.703c6.78,0 10.505,-5.771 10.505,-10.774c0,-0.167 0,-0.326 -0.008,-0.494c0.722,-0.529 1.342,-1.2 1.841,-1.967Z"></path></svg></span></a></li>
                                    <li className="item"><a target="_blank" rel="nofollow" className="telegram" href="https://t.me/joinchat/GrjMXRAZ1If8xaj_1bDHzw" title="en Telegram"><span className="icono"><svg viewBox="0 0 24 24"><path d="M18.384,22.779c0.322,0.228 0.737,0.285 1.107,0.145c0.37,-0.141 0.642,-0.457 0.724,-0.84c0.869,-4.084 2.977,-14.421 3.768,-18.136c0.06,-0.28 -0.04,-0.571 -0.26,-0.758c-0.22,-0.187 -0.525,-0.241 -0.797,-0.14c-4.193,1.552 -17.106,6.397 -22.384,8.35c-0.335,0.124 -0.553,0.446 -0.542,0.799c0.012,0.354 0.25,0.661 0.593,0.764c2.367,0.708 5.474,1.693 5.474,1.693c0,0 1.452,4.385 2.209,6.615c0.095,0.28 0.314,0.5 0.603,0.576c0.288,0.075 0.596,-0.004 0.811,-0.207c1.216,-1.148 3.096,-2.923 3.096,-2.923c0,0 3.572,2.619 5.598,4.062Zm-11.01,-8.677l1.679,5.538l0.373,-3.507c0,0 6.487,-5.851 10.185,-9.186c0.108,-0.098 0.123,-0.262 0.033,-0.377c-0.089,-0.115 -0.253,-0.142 -0.376,-0.064c-4.286,2.737 -11.894,7.596 -11.894,7.596Z"></path></svg></span></a></li>
                                    <li className="item"><a target="_blank" rel="nofollow" className="youtube" href="https://www.youtube.com/user/motorpuntoes?sub_confirmation=1" title="en YouTube"><span className="icono"><svg viewBox="0 0 24 24"><path d="M12.004,10.495c0.005,0 3.198,0 5.329,0.15c0.298,0.034 0.947,0.037 1.527,0.632c0.457,0.454 0.606,1.483 0.606,1.483c0,0 0.151,1.209 0.151,2.418l0,1.134c0,1.208 -0.151,2.418 -0.151,2.418c0,0 -0.149,1.028 -0.606,1.481c-0.579,0.595 -1.229,0.599 -1.527,0.633c-2.132,0.152 -5.333,0.156 -5.333,0.156c0,0 -3.961,-0.035 -5.181,-0.15c-0.338,-0.062 -1.099,-0.043 -1.68,-0.639c-0.456,-0.452 -0.605,-1.481 -0.605,-1.481c0,0 -0.151,-1.21 -0.151,-2.418l0,-1.134c0,-1.209 0.151,-2.418 0.151,-2.418c0,0 0.149,-1.029 0.606,-1.483c0.58,-0.595 1.228,-0.598 1.526,-0.632c2.132,-0.15 5.326,-0.15 5.331,-0.15l0.007,0Zm-0.622,8.495l0,-5.021l-0.925,0l0,3.837c-0.205,0.286 -0.398,0.427 -0.583,0.427c-0.123,0 -0.194,-0.073 -0.218,-0.214c-0.007,-0.029 -0.007,-0.14 -0.007,-0.353l0,-3.697l-0.923,0l0,3.97c0,0.354 0.03,0.594 0.08,0.747c0.093,0.254 0.299,0.375 0.596,0.375c0.339,0 0.689,-0.204 1.056,-0.619l0,0.548l0.924,0Zm1.724,0l0,-0.486c0.308,0.376 0.626,0.557 0.955,0.557c0.367,0 0.614,-0.192 0.737,-0.566c0.063,-0.213 0.094,-0.548 0.094,-1.01l0,-2.001c0,-0.476 -0.031,-0.807 -0.093,-1.01c-0.124,-0.375 -0.371,-0.567 -0.738,-0.567c-0.34,0 -0.657,0.183 -0.955,0.544l0,-2.2l-0.924,0l0,6.739l0.924,0Zm5.222,-2.345l0,-1.041c0,-0.536 -0.093,-0.928 -0.286,-1.183c-0.259,-0.344 -0.626,-0.514 -1.09,-0.514c-0.472,0 -0.84,0.171 -1.106,0.514c-0.196,0.255 -0.286,0.647 -0.286,1.183l0,1.759c0,0.534 0.1,0.93 0.297,1.181c0.266,0.344 0.634,0.516 1.117,0.516c0.483,0 0.862,-0.181 1.117,-0.546c0.114,-0.162 0.188,-0.348 0.217,-0.544c0.008,-0.092 0.02,-0.294 0.02,-0.586l0,-0.134l-0.943,0c0,0.366 -0.012,0.568 -0.02,0.618c-0.053,0.243 -0.186,0.365 -0.413,0.365c-0.317,0 -0.472,-0.233 -0.472,-0.698l0,-0.89l1.848,0Zm-9.482,-3.446l0,-0.948l-3.22,0l0,0.947l1.086,0l0,5.792l1.028,0l0,-5.791l1.106,0Zm4.744,1.345c-0.153,0 -0.307,0.072 -0.461,0.22l0,3.062c0.154,0.152 0.308,0.225 0.461,0.225c0.265,0 0.401,-0.225 0.401,-0.678l0,-2.143c0,-0.454 -0.136,-0.686 -0.401,-0.686l0,0Zm3.376,0c-0.309,0 -0.464,0.232 -0.464,0.696l0,0.465l0.924,0l0,-0.465c0,-0.464 -0.154,-0.696 -0.46,-0.696Zm-0.174,-9.81l0,5.076l-0.932,0l0,-0.554c-0.373,0.42 -0.727,0.624 -1.067,0.624c-0.3,0 -0.508,-0.121 -0.602,-0.377c-0.052,-0.154 -0.082,-0.396 -0.082,-0.755l0,-4.014l0.93,0l0,3.738c0,0.215 0,0.327 0.012,0.357c0.02,0.142 0.094,0.215 0.219,0.215c0.187,0 0.383,-0.143 0.589,-0.43l0,-3.88l0.933,0Zm-4.779,5.146c0.468,0 0.83,-0.174 1.089,-0.519c0.195,-0.254 0.29,-0.654 0.29,-1.194l0,-1.778c0,-0.541 -0.095,-0.938 -0.29,-1.194c-0.259,-0.348 -0.621,-0.52 -1.089,-0.52c-0.466,0 -0.83,0.172 -1.087,0.52c-0.199,0.256 -0.292,0.653 -0.292,1.194l0,1.778c0,0.54 0.093,0.94 0.292,1.194c0.257,0.345 0.621,0.518 1.087,0.518l0,0.001Zm-2.937,-4.206l0.706,-2.674l1.047,0l-1.245,4.051l0,2.758l-1.035,0l0,-2.758c-0.094,-0.488 -0.302,-1.204 -0.633,-2.153c-0.219,-0.633 -0.445,-1.266 -0.665,-1.898l1.09,0l0.735,2.674Zm2.491,0.531c0,-0.469 0.145,-0.704 0.446,-0.704c0.3,0 0.444,0.235 0.444,0.704l0,2.135c0,0.47 -0.144,0.704 -0.444,0.704c-0.3,0 -0.446,-0.235 -0.446,-0.704l0,-2.135Z"></path></svg></span></a></li>
                                    <li className="item"><a target="_blank" rel="nofollow" className="flipboard" href="https://flipboard.com/@motorpuntoes" title="en Flipboard"><span className="icono"><svg viewBox="0 0 24 24"><path d="M9.636,3l11.364,0l0,6.629l-5.682,0l0,5.682l-5.682,0l0,5.689l-6.636,0l0,-18l6.636,0Zm-5.689,0.947l0,16.106l4.742,0l0,-16.106c-1.581,0 -3.161,0 -4.742,0Zm5.689,5.682l0,4.735l4.735,0l0,-4.735c-1.578,0 -3.156,0 -4.735,0Zm0,-5.682l0,4.735l10.417,0l0,-4.735c-3.472,0 -6.945,0 -10.417,0Z"></path></svg></span></a></li>
                                    <li className="item"><a target="_blank" rel="nofollow" className="linkedin" href="https://www.linkedin.com/company/motor-internet-s-l-/about/" title="en Linkedin"><span className="icono"><svg viewBox="0 0 24 24"><path d="M21,21H17V14.25C17,13.19 15.81,12.31 14.75,12.31C13.69,12.31 13,13.19 13,14.25V21H9V9H13V11C13.66,9.93 15.36,9.24 16.5,9.24C19,9.24 21,11.28 21,13.75V21M7,21H3V9H7V21M5,3A2,2 0 0,1 7,5A2,2 0 0,1 5,7A2,2 0 0,1 3,5A2,2 0 0,1 5,3Z"></path></svg></span></a></li>
                                    <li className="item"><a target="_blank" rel="nofollow" className="rss" href="http://feeds.feedburner.com/motorpuntoes" title="en RSS"><span className="icono"><svg viewBox="0 0 24 24"><path d="M5.522,15.956c1.383,0 2.522,1.139 2.522,2.522c0,1.365 -1.134,2.522 -2.522,2.522c-1.365,0 -2.522,-1.157 -2.522,-2.522c0,-1.383 1.138,-2.522 2.522,-2.522m-2.522,-12.956c9.875,0 18,8.125 18,18l-3.274,0c0,-8.079 -6.647,-14.726 -14.726,-14.726l0,-3.274m0,6.548c6.283,0 11.452,5.169 11.452,11.452l-3.273,0c0,-4.487 -3.692,-8.179 -8.179,-8.179l0,-3.273Z"></path></svg></span></a></li>
                                    <li className="item"><a target="_blank" rel="nofollow" className="whatsapp" href="https://api.whatsapp.com/send?text=Motor.es%20https://www.motor.es" title="en Whatsapp"><span className="icono"><svg viewBox="0 0 24 24"><path d="M16.75,13.96C17,14.09 17.16,14.16 17.21,14.26C17.27,14.37 17.25,14.87 17,15.44C16.8,16 15.76,16.54 15.3,16.56C14.84,16.58 14.83,16.92 12.34,15.83C9.85,14.74 8.35,12.08 8.23,11.91C8.11,11.74 7.27,10.53 7.31,9.3C7.36,8.08 8,7.5 8.26,7.26C8.5,7 8.77,6.97 8.94,7H9.41C9.56,7 9.77,6.94 9.96,7.45L10.65,9.32C10.71,9.45 10.75,9.6 10.66,9.76L10.39,10.17L10,10.59C9.88,10.71 9.74,10.84 9.88,11.09C10,11.35 10.5,12.18 11.2,12.87C12.11,13.75 12.91,14.04 13.15,14.17C13.39,14.31 13.54,14.29 13.69,14.13L14.5,13.19C14.69,12.94 14.85,13 15.08,13.08L16.75,13.96M12,2A10,10 0 0,1 22,12A10,10 0 0,1 12,22C10.03,22 8.2,21.43 6.65,20.45L2,22L3.55,17.35C2.57,15.8 2,13.97 2,12A10,10 0 0,1 12,2M12,4A8,8 0 0,0 4,12C4,13.72 4.54,15.31 5.46,16.61L4.5,19.5L7.39,18.54C8.69,19.46 10.28,20 12,20A8,8 0 0,0 20,12A8,8 0 0,0 12,4Z"></path></svg></span></a></li>
                                </ul>
                            </div>
                            <div className="items-group">
                                <ul className="list-items full-width">
                                    <li className="item icon-item"><a href="https://www.motor.es/equipo" title="Equipo"><span className="icono"><svg viewBox="0 0 24 24"><path d="M17.997 18h-11.995l-.002-.623c0-1.259.1-1.986 1.588-2.33 1.684-.389 3.344-.736 2.545-2.209-2.366-4.363-.674-6.838 1.866-6.838 2.491 0 4.226 2.383 1.866 6.839-.775 1.464.826 1.812 2.545 2.209 1.49.344 1.589 1.072 1.589 2.333l-.002.619zm4.811-2.214c-1.29-.298-2.49-.559-1.909-1.657 1.769-3.342.469-5.129-1.4-5.129-1.265 0-2.248.817-2.248 2.324 0 3.903 2.268 1.77 2.246 6.676h4.501l.002-.463c0-.946-.074-1.493-1.192-1.751zm-22.806 2.214h4.501c-.021-4.906 2.246-2.772 2.246-6.676 0-1.507-.983-2.324-2.248-2.324-1.869 0-3.169 1.787-1.399 5.129.581 1.099-.619 1.359-1.909 1.657-1.119.258-1.193.805-1.193 1.751l.002.463z"></path></svg></span><span>Equipo</span></a></li>
                                    <li className="item icon-item"><a href="https://www.motor.es/contacto-motorpuntoes" title="Contacto"><span className="icono"><svg viewBox="0 0 24 24"><path d="M12 12.713l-11.985-9.713h23.971l-11.986 9.713zm-5.425-1.822l-6.575-5.329v12.501l6.575-7.172zm10.85 0l6.575 7.172v-12.501l-6.575 5.329zm-1.557 1.261l-3.868 3.135-3.868-3.135-8.11 8.848h23.956l-8.11-8.848z"></path></svg></span><span>Contacto</span></a></li>
                                    <li className="item icon-item"><a rel="nofollow" target="_blank" href="https://www.youtube.com/user/motorpuntoes?sub_confirmation=1" title="Motor.es en Youtube"><span className="icono"><svg viewBox="0 0 24 24"><path d="M19.615 3.184c-3.604-.246-11.631-.245-15.23 0-3.897.266-4.356 2.62-4.385 8.816.029 6.185.484 8.549 4.385 8.816 3.6.245 11.626.246 15.23 0 3.897-.266 4.356-2.62 4.385-8.816-.029-6.185-.484-8.549-4.385-8.816zm-10.615 12.816v-8l8 3.993-8 4.007z"></path></svg></span><span>Motor.es en Youtube</span></a></li>
                                    <li className="item icon-item"><a rel="nofollow" target="_blank" href="https://play.google.com/store/apps/details?id=es.motor.app" title="Descarga nuestra App"><span className="icono"><svg viewBox="0 0 24 24"><path d="M6.02 7.389c.399-.285.85-.417 1.292-.417.944 0 1.852.6 2.15 1.599-.382-.294-.83-.437-1.281-.437-.458 0-.919.147-1.321.434-.799.57-1.153 1.541-.845 2.461-1.242-.89-1.247-2.747.005-3.64zm3.741 12.77c.994.334 4.071 1.186 7.635 3.841l6.604-4.71c-1.713-2.402-1.241-4.082-2.943-6.468-1.162-1.628-1.827-1.654-3.037-1.432l.599.84c.361.507-.405 1.05-.764.544l-.534-.75c-.435-.609-1.279-.229-2.053-.051l.727 1.019c.36.505-.403 1.051-.764.544l-.629-.882c-.446-.626-1.318-.208-2.095-.01l.769 1.078c.363.508-.405 1.049-.764.544l-3.118-4.366c-.968-1.358-3.088.083-2.086 1.489l4.605 6.458c-.494-.183-1.363-.349-1.93-.349-1.754 0-2.429 1.92-.222 2.661zm-3.286-2.159h-4.475v-14h10v6.688l2-.471v-8.217c0-1.104-.895-2-2-2h-10c-1.105 0-2 .896-2 2v18.678c-.001 2.213 3.503 3.322 7.005 3.322 1.812 0 3.619-.299 4.944-.894-2.121-.946-6.378-1.576-5.474-5.106z"></path></svg></span><span>Descarga nuestra App</span></a></li>
                                </ul>
                            </div>
                            <div className="items-group"><p className="group-title">Mucho más en Motor.es</p>
                                <ul className="list-items full-width">
                                    <li className="item drop-down"><a className="drop-down-link" href="#" title="Esto te puede interesar">Esto te puede interesar
                                        <p className="link-description">Recopilación de contenidos muy
                                            interesante...</p></a>
                                        <ul id="drop_drown_0" className="list-items drop-down-list list list-tags">
                                            <li className="item"><a title="" href="https://www.motor.es/tag/amores-de-juventud">Amores de juventud</a></li>
                                            <li className="item"><a title="" href="https://www.motor.es/tag/coches-imposibles">Coches imposibles</a></li>
                                            <li className="item"><a title="" href="https://www.motor.es/tag/coches-rarunos">Coches rarunos</a></li>
                                            <li className="item"><a title="" href="https://www.motor.es/noticias/indice-velocidad-neumatico-donde-ver-significado-201963514.html">Índice velocidad neumáticos</a></li>
                                            <li className="item"><a title="" href="https://www.motor.es/tag/exclusiva">Exclusivas</a></li>
                                            <li className="item"><a title="" href="https://www.motor.es/noticias/diesel-o-gasolina-201963517.html">Diésel o Gasolina</a></li>
                                            <li className="item"><a title="" href="https://www.motor.es/noticias/renting-o-compra-201963368.html">Renting o Compra</a></li>
                                        </ul>
                                    </li>
                                    <li className="item drop-down"><a className="drop-down-link" href="#" title="En competición">En competición
                                        <p className="link-description">Si te gusta el motorsport, aquí tienes todo lo que necesitas...</p></a>
                                        <ul id="drop_drown_1" className="list-items drop-down-list list list-tags">
                                            <li className="item"><a title="" href="https://www.motor.es/formula-1/">Fórmula 1</a></li>
                                            <li className="item"><a title="" href="https://www.motor.es/motorsport/wrc/">WRC</a></li>
                                            <li className="item"><a title="" href="https://www.motor.es/motorsport/wec/">WEC</a></li>
                                            <li className="item"><a title="" href="https://www.motor.es/motorsport/wtcr/">WTCR</a></li>
                                            <li className="item"><a title="" href="https://www.motor.es/motorsport/dtm/">DTM</a></li>
                                            <li className="item"><a title="" href="https://www.motor.es/motorsport/dakar/">Dakar</a></li>
                                            <li className="item"><a title="" href="https://www.motor.es/motorsport/formula-e/">Fórmula E</a></li>
                                            <li className="item"><a title="" href="https://www.motor.es/motorsport/">Todas</a></li>
                                        </ul>
                                    </li>
                                    <li className="item drop-down"><a className="drop-down-link" href="#" title="Servicios">Servicios
                                        <p className="link-description">En Motor.es te ofrecemos algunos servicios que
                                            te ayudarán en la compra venta de tu coche...</p></a>
                                        <ul id="drop_drown_2" className="list-items drop-down-list list list-tags">
                                            <li className="item"><a title="" href="https://www.motor.es/tasar-coche">Tasar coche</a></li>
                                            <li className="item"><a title="" href="https://ad.doubleclick.net/ddm/clk/458754881;263259599;j">Comparador seguros</a></li>
                                            <li className="item"><a title="" href="https://financiacion.motor.es/?utm_source=MTRES&amp;utm_medium=whitelabel&amp;utm_content=cabecera&amp;utm_campaign=pr%C3%A9stamos">Financiación</a></li>
                                        </ul>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div className="menu-footer"><span className="footer-text">© Motor.es - Todos los derechos reservados</span>
                        </div>
                    </div>
                </section>
            </div>
    );
    }
    }

    export default Header;