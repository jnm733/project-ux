import * as React from 'react';

interface IListCarUsedItemProps {
    title: string,
    pvp: string,
    image: string,
    info: string,
    modal: boolean,
    onModalClick: (modal: boolean) => any;
}

interface IListCarUsedItemState {

}

class ListCarUsedItem extends React.Component<IListCarUsedItemProps, IListCarUsedItemState> {
    constructor(props: IListCarUsedItemProps) {
        super(props);
    }

    public render() {
        const onClick = () => {
            this.props.onModalClick(!!!this.props.modal)
        }
        return (
            <li className="item">
                <a onClick={onClick} title={this.props.title}>
                    <a className="imagen" onClick={onClick} title={this.props.title}>
                        <img src={'img/ocasion/'+this.props.image+'.jpg'} alt={this.props.title}></img>
                    </a>
                    <div className="content">
                        <h3 className="title">
                            {this.props.title}
                        </h3>
                        <div className="data">
                            <p className="info">{this.props.info}</p>
                            <span className="precio">
                                <span className="oferta"> {this.props.pvp} €<span className="icono"><svg viewBox="0 0 24 24"><path d="M8.59,16.58L13.17,12L8.59,7.41L10,6L16,12L10,18L8.59,16.58Z"></path></svg></span></span>
                            </span>
                        </div>
                        <a className="solicitar-oferta-link" onClick={onClick}><p className="solicitar-oferta-label">Solicitar oferta</p></a>
                    </div>
                </a>
            </li>
        );
    }
}



export default ListCarUsedItem;