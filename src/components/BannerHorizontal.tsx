import * as React from 'react';
import banner_superior from "../assets/img/banners/megabanner-superior.jpg";

interface IBannerHorizontalProps {

}

interface IBannerHorizontalState {

}

class LeftColumn extends React.Component<IBannerHorizontalProps, IBannerHorizontalState> {
    constructor(props: IBannerHorizontalProps) {
        super(props);
    }

    public render() {
        return (
            <div className="megabanner">
                <span className="creatividad">
                    <img src={banner_superior}></img>
                </span>
            </div>
    );
    }
    }

    export default LeftColumn;