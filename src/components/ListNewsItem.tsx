import * as React from 'react';

interface IListNewsItemProps {
    title: string,
    description: string,
    author: string,
    tags?: String[],
    image: string
}

interface IListNewsItemState {

}

class ListModelItem extends React.Component<IListNewsItemProps, IListNewsItemState> {
    constructor(props: IListNewsItemProps) {
        super(props);
    }

    public render() {
        return (
            <article className="articulo">
                <a className="imagen" href="#" title={this.props.title}>
                    <img src={'img/noticias/'+this.props.image+'.jpg'} alt={this.props.title}></img>
                </a>
                <span className="cartela">Novedades</span>
                <span className="textos">
                    <footer className="data">
                        <span className="autor">por {this.props.author}</span>
                    </footer>
                    <h3>
                        <a href="#" title={this.props.title}>
                            <span className="titular xs">{this.props.title}</span>
                        </a>
                    </h3>
                    <p className="entradilla">{this.props.description}</p>
                    {this.props.tags ?
                        <ul className="etiquetas">
                            {this.props.tags.map(tag => <li><a href="#">{tag}</a></li>)}
                        </ul>
                        : null}
                    <span className="botonera">
                        <span className="boton fantasma" data-compartir-articulo="data-compartir-articulo">
                            <span className="icono"><svg viewBox="0 0 24 24"><path d="M18,16.08C17.24,16.08 16.56,16.38 16.04,16.85L8.91,12.7C8.96,12.47 9,12.24 9,12C9,11.76 8.96,11.53 8.91,11.3L15.96,7.19C16.5,7.69 17.21,8 18,8A3,3 0 0,0 21,5A3,3 0 0,0 18,2A3,3 0 0,0 15,5C15,5.24 15.04,5.47 15.09,5.7L8.04,9.81C7.5,9.31 6.79,9 6,9A3,3 0 0,0 3,12A3,3 0 0,0 6,15C6.79,15 7.5,14.69 8.04,14.19L15.16,18.34C15.11,18.55 15.08,18.77 15.08,19C15.08,20.61 16.39,21.91 18,21.91C19.61,21.91 20.92,20.61 20.92,19A2.92,2.92 0 0,0 18,16.08Z"></path></svg></span>
                        </span>
                    </span>
                </span>
            </article>
    );
    }
    }

    export default ListModelItem;