import * as React from 'react';
import logo_motor from '../assets/img/motor-es-negativo.svg';

interface IFooterProps {

}

interface IFooterState {

}

class Footer extends React.Component<IFooterProps, IFooterState> {
    constructor(props: IFooterProps) {
        super(props);
    }

    public render() {
        return (
            <footer className="pie-pagina">
                <aside className="seccion">
                    <h4 className="titulo-seccion">Lo más buscado en motor.es</h4>
                    <ul className="modelos">
                        <li><a href="#" title="Audi A3">Audi A3</a></li>
                        <li><a href="#" title="BMW Serie 5">BMW Serie 5</a></li>
                        <li><a href="#" title="Citroen DS5">Citroen DS5</a></li>
                        <li><a href="#" title="Ford Mondeo">Ford Mondeo</a></li>
                        <li><a href="#" title="Fiat Bravo">Fiat Bravo</a></li>
                        <li><a href="#" title="Fiat 500">Fiat 500</a></li>
                        <li><a href="#" title="Mazda 5">Mazda 5</a></li>
                        <li><a href="#" title="Audi A5">Audi A5</a></li>
                        <li><a href="#" title="Seat León">Seat León</a></li>
                        <li><a href="#" title="Honda Civic">Honda Civic</a></li>
                        <li><a href="#" title="Mini Cabrio">Mini Cabrio</a></li>
                        <li><a href="#" title="Mini 5 Puertas">Mini 5 Puertas</a></li>
                        <li><a href="#" title="Nissan Pulsar">Nissan Pulsar</a></li>
                        <li><a href="#" title="Opel Corsa">Opel Corsa</a></li>
                    </ul>
                </aside>
                <aside className="seccion">
                    <h4 className="titulo-seccion">Coches de segunda mano</h4>
                    <ul className="marcas">
                        <li><a href="#" title="Alfa Romeo"><img src="img/marcas/alfa-romeo-small.jpg" alt="Alfa Romeo"></img></a>
                        </li>
                        <li><a href="#" title="Audi"><img src="img/marcas/audi-small.jpg" alt="Audi"></img></a></li>
                        <li><a href="#" title="BMW"><img src="img/marcas/bmw-small.jpg" alt="BMW"></img></a></li>
                        <li><a href="#" title="Chevrolet"><img src="img/marcas/chevrolet-small.jpg" alt="Chevrolet"></img></a>
                        </li>
                        <li><a href="#" title="Citroen"><img src="img/marcas/citroen-small.jpg" alt="Citroen"></img></a></li>
                        <li><a href="#" title="Mercedes"><img src="img/marcas/mercedes-small.jpg" alt="Mercedes"></img></a>
                        </li>
                        <li><a href="#" title="Fiat"><img src="img/marcas/fiat-small.jpg" alt="Fiat"></img></a></li>
                        <li><a href="#" title="Ford"><img src="img/marcas/ford-small.jpg" alt="Ford"></img></a></li>
                        <li><a href="#" title="Skoda"><img src="img/marcas/skoda-small.jpg" alt="Skoda"></img></a></li>
                        <li><a href="#" title="Renault"><img src="img/marcas/renault-small.jpg" alt="Renault"></img></a></li>
                        <li><a href="#" title="Nissan"><img src="img/marcas/nissan-small.jpg" alt="Nissan"></img></a></li>
                        <li><a href="#" title="Toyota"><img src="img/marcas/toyota-small.jpg" alt="Toyota"></img></a></li>
                        <li><a href="#" title="Mazda"><img src="img/marcas/mazda-small.jpg" alt="Mazda"></img></a></li>
                        <li><a href="#" title="Honda"><img src="img/marcas/honda-small.jpg" alt="Honda"></img></a></li>
                        <li><a href="#" title="Volkswagen"><img src="img/marcas/volkswagen-small.jpg" alt="Volkswagen"></img></a>
                        </li>
                        <li><a href="#" title="Lexus"><img src="img/marcas/lexus-small.jpg" alt="Lexus"></img></a></li>
                        <li><a href="#" title="Seat"><img src="img/marcas/seat-small.jpg" alt="Seat"></img></a></li>
                        <li><a href="#" title="Mini"><img src="img/marcas/mini-small.jpg" alt="Mini"></img></a></li>
                        <li><a href="#" title="Jaguar"><img src="img/marcas/jaguar-small.jpg" alt="Jaguar"></img></a></li>
                        <li><a href="#" title="Opel"><img src="img/marcas/opel-small.jpg" alt="Opel"></img></a></li>
                        <li><a href="#" title="Land Rover"><img src="img/marcas/land-rover-small.jpg" alt="Land Rover"></img></a>
                        </li>
                        <li><a href="#" title="Infinity"><img src="img/marcas/infinity-small.jpg" alt="Infinity"></img></a>
                        </li>
                        <li><a href="#" title="Volvo"><img src="img/marcas/volvo-small.jpg" alt="Volvo"></img></a></li>
                        <li><a href="#" title="Smart"><img src="img/marcas/smart-small.jpg" alt="Smart"></img></a></li>
                    </ul>
                    <ul className="provincias">
                        <li><a href="#" title="A Coruña">A Coruña</a></li>
                        <li><a href="#" title="Álava">Álava</a></li>
                        <li><a href="#" title="Albacete">Albacete</a></li>
                        <li><a href="#" title="Alicante">Alicante</a></li>
                        <li><a href="#" title="Almería">Almería</a></li>
                        <li><a href="#" title="Asturias">Asturias</a></li>
                        <li><a href="#" title="Ávila">Ávila</a></li>
                        <li><a href="#" title="Badajoz">Badajoz</a></li>
                        <li><a href="#" title="Barcelona">Barcelona</a></li>
                        <li><a href="#" title="Burgos">Burgos</a></li>
                        <li><a href="#" title="Cáceres">Cáceres</a></li>
                        <li><a href="#" title="Cádiz">Cádiz</a></li>
                        <li><a href="#" title="Cantabria">Cantabria</a></li>
                        <li><a href="#" title="Castellón">Castellón</a></li>
                        <li><a href="#" title="Ceuta">Ceuta</a></li>
                        <li><a href="#" title="Ciudad Real">Ciudad Real</a></li>
                        <li><a href="#" title="Cordoba">Cordoba</a></li>
                        <li><a href="#" title="Cuenca">Cuenca</a></li>
                        <li><a href="#" title="Girona">Girona</a></li>
                        <li><a href="#" title="Granada">Granada</a></li>
                        <li><a href="#" title="Guadalajara">Guadalajara</a></li>
                        <li><a href="#" title="Guipúzcoa">Guipúzcoa</a></li>
                        <li><a href="#" title="Huelva">Huelva</a></li>
                        <li><a href="#" title="Huesca">Huesca</a></li>
                        <li><a href="#" title="Baleares">Baleares</a></li>
                        <li><a href="#" title="Jaén">Jaén</a></li>
                        <li><a href="#" title="La Rioja">La Rioja</a></li>
                        <li><a href="#" title="Las Palmas">Las Palmas</a></li>
                        <li><a href="#" title="León">León</a></li>
                        <li><a href="#" title="LLeida">LLeida</a></li>
                        <li><a href="#" title="Lugo">Lugo</a></li>
                        <li><a href="#" title="Madrid">Madrid</a></li>
                        <li><a href="#" title="Málaga">Málaga</a></li>
                        <li><a href="#" title="Melilla">Melilla</a></li>
                        <li><a href="#" title="Murcia">Murcia</a></li>
                        <li><a href="#" title="Navarra">Navarra</a></li>
                    </ul>
                    <a className="todas-marcas" href="#" title="Ver todas las marcas">Ver todas las marcas</a>
                </aside>
                <div className="azul">
                    <div className="seccion">
                        <ul className="enlaces">
                            <li><a href="#" title="Guía de seguridad">Guía de seguridad</a></li>
                            <li><a href="#" title="Concesionarios segunda mano">Concesionarios segunda mano</a></li>
                            <li><a href="#" title="Comparador seguros">Comparador seguros</a></li>
                            <li><a href="#" title="Tasar coche">Tasar coche</a></li>
                            <li><a href="#" title="Equipo">Equipo</a></li>
                        </ul>
                        <ul className="enlaces">
                            <li><a href="#" title="Aviso legal y condiciones de uso">Aviso legal y condiciones de
                                uso</a></li>
                            <li><a href="#" title="Política de privacidad">Política de privacidad</a></li>
                            <li><a href="#" title="Política de cookies">Política de cookies</a></li>
                            <li><a href="#" title="Contacto">Contacto</a></li>
                            <li><a href="#" title="Preguntas y respuestas">Preguntas y respuestas</a></li>
                        </ul>
                        <div className="social"><a className="logo-motor" href="/" title="motor.es"><img
                            src={logo_motor} alt="motor.es"></img></a>RSS<a className="boton-cuadrado rss"
                                                                                     href="#" title="RSS"><span
                            className="icono"><svg viewBox="0 0 24 24"><path
                            d="M5.522,15.956c1.383,0 2.522,1.139 2.522,2.522c0,1.365 -1.134,2.522 -2.522,2.522c-1.365,0 -2.522,-1.157 -2.522,-2.522c0,-1.383 1.138,-2.522 2.522,-2.522m-2.522,-12.956c9.875,0 18,8.125 18,18l-3.274,0c0,-8.079 -6.647,-14.726 -14.726,-14.726l0,-3.274m0,6.548c6.283,0 11.452,5.169 11.452,11.452l-3.273,0c0,-4.487 -3.692,-8.179 -8.179,-8.179l0,-3.273Z"></path></svg></span></a>Síguenos<a
                            className="boton-cuadrado" href="#" title="en Facebook"><span className="icono"><svg
                            viewBox="0 0 24 24"><path
                            d="M16.5,3l0,3.375l-2.25,0c-0.617,0 -1.125,0.508 -1.125,1.125l0,2.25l3.375,0l0,3.375l-3.375,0l0,7.875l-3.375,0l0,-7.875l-2.25,0l0,-3.375l2.25,0l0,-2.813c0,-2.182 1.766,-3.937 3.938,-3.937l2.812,0Z"></path></svg></span></a><a
                            className="boton-cuadrado" href="#" title="en Twitter"><span className="icono"><svg
                            viewBox="0 0 24 24"><path
                            d="M21,6.265c-0.663,0.309 -1.377,0.511 -2.117,0.609c0.758,-0.468 1.343,-1.209 1.618,-2.1c-0.714,0.441 -1.506,0.75 -2.34,0.926c-0.68,-0.759 -1.635,-1.2 -2.719,-1.2c-2.022,0 -3.674,1.694 -3.674,3.785c0,0.3 0.034,0.591 0.094,0.865c-3.063,-0.159 -5.79,-1.668 -7.606,-3.953c-0.318,0.556 -0.499,1.209 -0.499,1.897c0,1.315 0.645,2.48 1.644,3.141c-0.611,0 -1.179,-0.176 -1.678,-0.441l0,0.027c0,1.835 1.273,3.37 2.96,3.714c-0.31,0.089 -0.637,0.133 -0.973,0.133c-0.232,0 -0.464,-0.027 -0.688,-0.071c0.465,1.491 1.815,2.603 3.442,2.629c-1.257,1.024 -2.848,1.624 -4.586,1.624c-0.293,0 -0.585,-0.018 -0.878,-0.053c1.635,1.077 3.579,1.703 5.662,1.703c6.78,0 10.505,-5.771 10.505,-10.774c0,-0.167 0,-0.326 -0.008,-0.494c0.722,-0.529 1.342,-1.2 1.841,-1.967Z"></path></svg></span></a><a
                            className="boton-cuadrado" href="#" title="en Google+"><span className="icono"><svg
                            viewBox="0 0 24 24"><path
                            d="M22,11.091l-1.818,0l0,-1.818l-1.818,0l0,1.818l-1.819,0l0,1.818l1.819,0l0,1.818l1.818,0l0,-1.818l1.818,0m-13.636,-1.818l0,2.182l3.636,0c-0.182,0.909 -1.091,2.727 -3.636,2.727c-2.182,0 -3.909,-1.818 -3.909,-4c0,-2.182 1.727,-4 3.909,-4c1.272,0 2.091,0.545 2.545,1l1.727,-1.636c-1.091,-1.091 -2.545,-1.728 -4.272,-1.728c-3.546,0 -6.364,2.819 -6.364,6.364c0,3.545 2.818,6.364 6.364,6.364c3.636,0 6.091,-2.546 6.091,-6.182c0,-0.455 0,-0.727 -0.091,-1.091l-6,0Z"></path></svg></span></a><a
                            className="boton-cuadrado" href="#" title="en YouTube"><span className="icono"><svg
                            viewBox="0 0 24 24"><path
                            d="M12.004,10.495c0.005,0 3.198,0 5.329,0.15c0.298,0.034 0.947,0.037 1.527,0.632c0.457,0.454 0.606,1.483 0.606,1.483c0,0 0.151,1.209 0.151,2.418l0,1.134c0,1.208 -0.151,2.418 -0.151,2.418c0,0 -0.149,1.028 -0.606,1.481c-0.579,0.595 -1.229,0.599 -1.527,0.633c-2.132,0.152 -5.333,0.156 -5.333,0.156c0,0 -3.961,-0.035 -5.181,-0.15c-0.338,-0.062 -1.099,-0.043 -1.68,-0.639c-0.456,-0.452 -0.605,-1.481 -0.605,-1.481c0,0 -0.151,-1.21 -0.151,-2.418l0,-1.134c0,-1.209 0.151,-2.418 0.151,-2.418c0,0 0.149,-1.029 0.606,-1.483c0.58,-0.595 1.228,-0.598 1.526,-0.632c2.132,-0.15 5.326,-0.15 5.331,-0.15l0.007,0Zm-0.622,8.495l0,-5.021l-0.925,0l0,3.837c-0.205,0.286 -0.398,0.427 -0.583,0.427c-0.123,0 -0.194,-0.073 -0.218,-0.214c-0.007,-0.029 -0.007,-0.14 -0.007,-0.353l0,-3.697l-0.923,0l0,3.97c0,0.354 0.03,0.594 0.08,0.747c0.093,0.254 0.299,0.375 0.596,0.375c0.339,0 0.689,-0.204 1.056,-0.619l0,0.548l0.924,0Zm1.724,0l0,-0.486c0.308,0.376 0.626,0.557 0.955,0.557c0.367,0 0.614,-0.192 0.737,-0.566c0.063,-0.213 0.094,-0.548 0.094,-1.01l0,-2.001c0,-0.476 -0.031,-0.807 -0.093,-1.01c-0.124,-0.375 -0.371,-0.567 -0.738,-0.567c-0.34,0 -0.657,0.183 -0.955,0.544l0,-2.2l-0.924,0l0,6.739l0.924,0Zm5.222,-2.345l0,-1.041c0,-0.536 -0.093,-0.928 -0.286,-1.183c-0.259,-0.344 -0.626,-0.514 -1.09,-0.514c-0.472,0 -0.84,0.171 -1.106,0.514c-0.196,0.255 -0.286,0.647 -0.286,1.183l0,1.759c0,0.534 0.1,0.93 0.297,1.181c0.266,0.344 0.634,0.516 1.117,0.516c0.483,0 0.862,-0.181 1.117,-0.546c0.114,-0.162 0.188,-0.348 0.217,-0.544c0.008,-0.092 0.02,-0.294 0.02,-0.586l0,-0.134l-0.943,0c0,0.366 -0.012,0.568 -0.02,0.618c-0.053,0.243 -0.186,0.365 -0.413,0.365c-0.317,0 -0.472,-0.233 -0.472,-0.698l0,-0.89l1.848,0Zm-9.482,-3.446l0,-0.948l-3.22,0l0,0.947l1.086,0l0,5.792l1.028,0l0,-5.791l1.106,0Zm4.744,1.345c-0.153,0 -0.307,0.072 -0.461,0.22l0,3.062c0.154,0.152 0.308,0.225 0.461,0.225c0.265,0 0.401,-0.225 0.401,-0.678l0,-2.143c0,-0.454 -0.136,-0.686 -0.401,-0.686l0,0Zm3.376,0c-0.309,0 -0.464,0.232 -0.464,0.696l0,0.465l0.924,0l0,-0.465c0,-0.464 -0.154,-0.696 -0.46,-0.696Zm-0.174,-9.81l0,5.076l-0.932,0l0,-0.554c-0.373,0.42 -0.727,0.624 -1.067,0.624c-0.3,0 -0.508,-0.121 -0.602,-0.377c-0.052,-0.154 -0.082,-0.396 -0.082,-0.755l0,-4.014l0.93,0l0,3.738c0,0.215 0,0.327 0.012,0.357c0.02,0.142 0.094,0.215 0.219,0.215c0.187,0 0.383,-0.143 0.589,-0.43l0,-3.88l0.933,0Zm-4.779,5.146c0.468,0 0.83,-0.174 1.089,-0.519c0.195,-0.254 0.29,-0.654 0.29,-1.194l0,-1.778c0,-0.541 -0.095,-0.938 -0.29,-1.194c-0.259,-0.348 -0.621,-0.52 -1.089,-0.52c-0.466,0 -0.83,0.172 -1.087,0.52c-0.199,0.256 -0.292,0.653 -0.292,1.194l0,1.778c0,0.54 0.093,0.94 0.292,1.194c0.257,0.345 0.621,0.518 1.087,0.518l0,0.001Zm-2.937,-4.206l0.706,-2.674l1.047,0l-1.245,4.051l0,2.758l-1.035,0l0,-2.758c-0.094,-0.488 -0.302,-1.204 -0.633,-2.153c-0.219,-0.633 -0.445,-1.266 -0.665,-1.898l1.09,0l0.735,2.674Zm2.491,0.531c0,-0.469 0.145,-0.704 0.446,-0.704c0.3,0 0.444,0.235 0.444,0.704l0,2.135c0,0.47 -0.144,0.704 -0.444,0.704c-0.3,0 -0.446,-0.235 -0.446,-0.704l0,-2.135Z"></path></svg></span></a>
                        </div>
                        <p className="legal">Motor.es cumple la Ley orgánica 15/1999 de 13 de diciembre de protección de
                            datos de carácter personal</p>
                    </div>
                </div>
            </footer>
    );
    }
    }

    export default Footer;