import * as React from 'react';
import banner_alto from '../assets/img/banners/robapaginas-alto.jpg';

interface IBannerVerticalProps {

}

interface IBannerVerticalState {

}

class LeftColumn extends React.Component<IBannerVerticalProps, IBannerVerticalState> {
    constructor(props: IBannerVerticalProps) {
        super(props);
    }

    public render() {
        return (
            <span className="divisor">
                <div className="robapaginas-portada">
                    <span className="creatividad">
                        <img src={ banner_alto }></img>
                    </span>
                </div>
            </span>
    );
    }
    }

    export default LeftColumn;