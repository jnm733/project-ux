import ListCarUsedItem from '../components/ListCarUsedItem';
import { connect } from 'react-redux';
import IGlobalState from '../state/globalState';
import { Dispatch } from 'redux';
import {HeaderActions} from "../actions/HeaderActions";

const mapStateToProps = (state: IGlobalState) => {
    return ({modal: state.modal})
}

const mapDispatchToProps = (dispatch: Dispatch) => ({
    onModalClick: (modal: boolean) => {
        dispatch({type: HeaderActions.MODAL_COLLAPSE, payload: modal});
    }
})

export default connect(mapStateToProps, mapDispatchToProps)(ListCarUsedItem);