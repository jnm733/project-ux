import React from 'react';
import './assets/css/estilos.css';
import './assets/css/custom.css';
import { Provider } from 'react-redux';
import { store } from './state/store';
import Header from './containers/Header'
import Footer from './components/Footer'
import Content from './containers/Content'

const App: React.FC = () => {
  return (
      <Provider store={store}>
        <Header />
        <Content />
        <Footer />
      </Provider>
  );
}

export default App;
