import { Action, createStore } from 'redux';
import IGlobalState, { initialState } from './globalState';
import { HeaderActions, IFilterCollapseAction } from '../actions/HeaderActions';

const reducer = (state: IGlobalState = initialState, action: Action) => {
    switch (action.type) {
      case HeaderActions.HEADER_COLLAPSE:
        const collapsedAction = action as IFilterCollapseAction;
        return {...state, collapsed: collapsedAction.payload}
    case HeaderActions.MODAL_COLLAPSE:
        const collapsedModalAction = action as IFilterCollapseAction;
        return {...state, modal: collapsedModalAction.payload}
    }
    return state;
  }

  export const store = createStore(reducer, initialState);