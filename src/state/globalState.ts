interface IGlobalState {
    make: string;
    collapsed: boolean;
    modal: boolean
}

export default IGlobalState;

export const initialState: IGlobalState = {
    collapsed: true,
    make: "Audi",
    modal: false
}