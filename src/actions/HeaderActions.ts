import { Action } from 'redux';

export enum HeaderActions {
    HEADER_COLLAPSE = "HEADER_COLLAPSE_LATERAL_MENU",
    MODAL_COLLAPSE = "MODAL_COLLAPSE",
}

export interface IFilterCollapseAction extends Action {
    payload: boolean;
}