## Refactoring Lean UX - Motor.es

Para inicializar el proyecto es necesario ejecutar en primer lugar:

### `npm install`

Con npm-install instalamos todas las dependencias globales y de desarrollo del proyecto.

Una vez instaladas todas las dependencias ejecutamos:

### `npm start`

Con npm-start se realiza la build del proyecto y se inicializa un servidor web local de desarrollo con el que podremos acceder a la web desde [http://localhost:3000](http://localhost:3000).
